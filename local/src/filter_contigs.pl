#!/usr/bin/perl
# Takes in input a fasta file and removes short sequences (below a given length)

use strict;
use warnings;
use Getopt::Long;
use Pod::Usage;
use Switch;

my $fasta;
my $length = 500;
my $help = 0;

GetOptions(
        'fasta=s' => \$fasta,
	'length=i' => \$length,
        'help|?'       => \$help
        ) or pod2usage(2);
pod2usage(1) if $help;
pod2usage(1) if (!defined($fasta));

open(FASTA, $fasta) or die "could not open input file\n";

my $seq = "";
my $tot = 0;
my $ID;

while (my $line = <FASTA>) {
	if ($line =~ /^\>/) {
		my $tmp = $line;
		if ((defined($ID)) and ($tot >= $length)) {
			print $ID;
			print $seq;
		}
		$tot = 0;
		$seq = "";
		$ID = $tmp;
	}
	else {
		chomp $line;
		$tot += length($line);
		$line .= "\n";
		$seq .= $line;
	}
}
close FASTA;

if ((defined($ID)) and ($tot >= $length)) {
	print $ID;
	print $seq;
}

__END__

=head1 GFF Statistics

filter_contigs.pl - Using this script

=head1 SYNOPSIS

perl filter_contigs.pl [--fasta <filename>] [--length <integer>] [--help]

Takes in input a fasta file and removes short sequences (below a given length)

  Options:
    --fasta <filename>	input fasta file
    --length <integer>	minimum sequence length, default 500
    --help		print this help message

output on standard output (redirect, if necessary)
