# Copyright 2019 Michele Vidotto <michele.vidotto@gmail.com>

log:
	mkdir -p $@

tmp:
	mkdir -p $@

SPAdes_out/scaffolds.fasta: input_dataset.yaml log tmp
	!threads
	$(call load_env); \
	spades.py \
	--threads "$$THREADNUM" \
	--tmp-dir $^3 \
	--careful \
	-o $(dir $@) \
	--dataset $< \
	2>&1 \
	| tee $^2/spades.log

scaffolds.fasta: SPAdes_out/scaffolds.fasta
	ln -sf $< $@

contigs.fasta: SPAdes_out/scaffolds.fasta
	ln -sf SPAdes_out/contigs.fasta $@

assembly_graph_with_scaffolds.gfa: SPAdes_out/scaffolds.fasta
	ln -sf SPAdes_out/assembly_graph_with_scaffolds.gfa $@

# filter for scaffolds or contigs length
$(FILTER_LENGTH)bp.%.fasta.gz: %.fasta
	module purge; \
	filter_contigs --fasta $< --length $(FILTER_LENGTH) \
	| gzip -c9 >$@

# get statistics
$(FILTER_LENGTH)bp.%.gam: $(FILTER_LENGTH)bp.%.fasta.gz
	module purge; \
	module load sw/assemblers/gam-ngs/default; \
	gam-n50 <(zcat $<) \
	| tr \\t \\n >$@
	
ALL += SPAdes_out/scaffolds.fasta \
	scaffolds.fasta \
	contigs.fasta \
	assembly_graph_with_scaffolds.gfa \
	1bp.scaffolds.fasta.gz 1bp.contigs.fasta.gz \
	1bp.scaffolds.gam 1bp.contigs.gam
	